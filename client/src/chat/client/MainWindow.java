package chat.client;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainWindow extends JFrame {


    private JButton ConnectButton;
    private JTextField NicknameTextField;
    private JTextField IPTextField;
    private JPanel panel;
    private JButton button1;
    private JButton button2;


    public class ConnectButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new ChatWindow(IPTextField.getText(),NicknameTextField.getText());
                }
            });

        }
    }

    public MainWindow(){
        this.getContentPane().add(panel);
        this.ConnectButton.addActionListener(new ConnectButtonListener());
    }

}
